\section{CAVE display}

\subsection{Introduction}

\begin{itemize}
\item \ParaView has basic support for visualization in CAVE-like/VR or \VE.
\item Like typical computing systems, \VE consist of peripheral displays (output) and input devices. However, unlike regular systems, \VE keep track of the physical location of their IO devices with respect to an assumed base coordinates in the physical room (room coordinates).
\item Typically \VE consist of multiple stereoscopic displays that are configured based on the room coordinates. These base coordinates also typically serve as the reference coordinate for tracked inputs commonly found in \VE.
\item \VR support in \ParaView includes the ability to configure displays (CAVE/Tiled Walls etc) and the ability to configure inputs using \VRPN/\VRUI.
\item \ParaView can operate in a Client / Server fashion. The VR/CAVE module leverage this by assigning different processes on the server to manage different displays. Displays are thus configured on the server side by passing a \texttt{*.pvx} configuration XML file during start-up.
\item The client acts as a central point of control. It can initiate the scene (visualization pipeline) and also relay tracked interactions to each server process managing the displays. The VR plugin on the client side enables this. Device and interaction style configurations are brought in through the \ParaView state file (This will also have a GUI component in the near future).
\end{itemize}

\subsection{Architecture and design overview }

The picture describes the data flow, control flow, synchronization and configuration aspects of \ParaView designed for \VE. The 4 compartments in the picture depicts the following ideas.

\begin{itemize}
\item We leverage the Render Server mechanism to drive the various displays in a CAVE/VE.
\item Devices and control signals are relayed through the client using a plugin (VRPlugin)
\item Rendering and inputs are synchronized using synchronous proxy mechanism inherent in \ParaView (PV).
\item Inputs are configured at the client end and displays are configured at the server end.
\end{itemize}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.9\linewidth]{Images/ParaView_UsersGuide_VRArch.png}
\label{fig:VRArch}
\end{center}
\end{figure}

\subsection{Process to build, configure and run \ParaView in VE (for \ParaView 4.0 or lower)}

\todo{FixReferences}
%http://www.paraview.org/Wiki/ParaView_VR_in_4.0

\subsection{Process to build, configure and run \ParaView in VE (for \ParaView 3.10 or lower)}

Enabling \ParaView for \VR is a five stage process.

\begin{itemize}
\item Building \ParaView with the required parameters.
\item Preparing the display configuration file.
\item Preparing the input configuration file.
\item Starting the server (with display \directory{config.pvx})
\item Starting the client (with input \directory{config.pvsm})
\end{itemize}

\subsubsection{Building}

Follow regular build instructions from
\todo{FixReferences}
%http://paraview.org/Wiki/ParaView:Build_And_Install
In addition following steps needs to be performed:

\begin{itemize}
\item Make sure Qt and MPI are installed cause they are required for building. If using \VRPN make sure it is build and installed as well.
\item When configuring cmake enable \texttt{BUILD\_SHARED\_LIB}, \texttt{PARAVIEW\_BUILD\_QT\_GUI}, \texttt{PARAVIEW\_USE\_MPI} and \texttt{PARAVIEW\_BUILD\_PLUGIN\_VRPlugin}.
\item On Apple and Linux platforms the Plugin uses \VRUI device daemon client as its default and on Windows \VRPN is the default.
\item If you want to enable the other simply enable \texttt{PARAVIEW\_USE\_VRPN} or \texttt{PARAVIEW\_USE\_VRUI}.
\item If \VRPN is not found in the default paths then \texttt{VRPN\_INCLUDE\_DIR} and \texttt{VRPN\_LIBRARY} may also need be set.
\end{itemize}

\subsubsection{Configuring Displays}

The PV server is responsible for configuring displays. The display configuration is stored on a \texttt{*.pvx} file.
\textbf{\ParaView has no concept of units and therefore user have to make sure that the configuration values are in the same measurements units as what tracker has producing. So for example if tracker data is in meters, then everything is considered in meters, if feet then feet becomes the unit for configuration.}

\paragraph{Structure of PVX Config File}
\begin{verbatim}
<?xml version="1.0" ?>
<pvx>
  <Process Type="server|dataserver|renderserver">
    <!--
      The only supported Type values are "server", "dataserver" or "renderserver".
      This controls which executable this configuration is applicable to.
      There can be multiple <Process /> elements in the same pvx file.
      ------------------------------------------------------
      |  Executable     | Applicable Process Type          |
      |  pvserver       | server, dataserver, renderserver |
      |  pvrenderserver | server, renderserver             |
      |  pvdataserver   | server, dataserver               |
      ------------------------------------------------------
     -->
    <EyeSeparation Value="0.065"/> <!-- Should be specified in the configuration file-->
    <Machine Name="hostname"
             Environment="DISPLAY=m1:0"
             LowerLeft="-1.0 -1.0 -1.0"
             LowerRight="1.0 -1.0 -1.0"
             UpperRight="1.0  1.0 -1.0">
      <!--
         There can be multiple <Machine> elements in a <Process> element,
         each one identifying the configuration for a process.
         All attributes are optional.
         name="hostname"
         Environment: the environment for the process.
         LowerLeft|LowerRight|UpperRight
       -->
    </Machine>
  </Process>
</pvx>
\end{verbatim}

\paragraph{Example Config.pvx}
The following example is for a six sided cave with origin at $(0,0,0)$:
\begin{verbatim}
<?xml version="1.0" ?>
<pvx>
 <Process Type="client" />
 <Process Type="server">
 <EyeSeparation Value="0.065"/>
  <Machine Name="Front"
           Environment="DISPLAY=:0"
           LowerLeft=" -1 -1 -1"
           LowerRight=" 1 -1 -1"
           UpperRight=" 1  1 -1" />
  <Machine Name="Right"
           Environment="DISPLAY=:0"
           LowerLeft="  1 -1 -1"
           LowerRight=" 1 -1  1"
           UpperRight=" 1  1  1" />
  <Machine Name="Left"
           Environment="DISPLAY=:0"
           LowerLeft=" -1 -1  1"
           LowerRight="-1 -1 -1"
           UpperRight="-1  1 -1"/>
  <Machine Name="Top"
           Environment="DISPLAY=:0"
           LowerLeft=" -1  1 -1"
           LowerRight=" 1  1 -1"
           UpperRight=" 1  1  1"/>
  <Machine Name="Bottom"
           Environment="DISPLAY=:0"
           LowerLeft=" -1 -1  1"
           LowerRight=" 1 -1  1"
           UpperRight=" 1 -1 -1"/>
  <Machine Name="Back"
           Environment="DISPLAY=:0"
           LowerLeft="  1 -1  1"
           LowerRight="-1 -1  1"
           UpperRight="-1  1  1"/>
  </Process>
</pvx>
\end{verbatim}
A sample PVX is provided in \directory{ParaView/Documentation/cave.pvx}. This can be used to play with different display configurations.

\paragraph{Notes on PVX file usage}

\begin{itemize}
\item PVX file should be specified as the last command line argument for any of the server processes.
\item The PVX file is typically specified for all the executables whose environment is being changed using the PVX file. In case of data-server/render-server configuration, if you are setting up the environment for the two processes groups, then the PVX file must be passed as a command line option to both the executables: \pvdataserver and \pvrenderserver.
\item When running in parallel the file is read on all nodes, hence it must be present on all nodes.
\item \ParaView has no concept of units; use tracker units as default unit for corner points values, eye separation and anything else that requires real world units.
\end{itemize}

\subsubsection{Configure Inputs}

Configuring inputs is a two step process:

\begin{itemize}
\item Define connections to one or more \VRPN/\VRUI servers.
\item Mapping data coming in from the server to \ParaView interactor styles.
\end{itemize}

\paragraph{Device Connections}
Connections are clients to \VRPN servers and or \VRUI device daemons, and are defined using XML as follows:
\begin{verbatim}
<VRConnectionManager>
    <VRUIConnection name="travel" address="localhost" port = "8555">
      <Button id="0" name="1"/>
      <Button id="1" name="2"/>
      <Button id="2" name="3"/>
      <Tracker id="0" name="head"/>
      <Tracker id="1" name="wand"/>
      <!-- you can also apply a transformation matrix to the tracking data: -->
    <TrackerTransform value="
     1 0 0 0
     0 1 0 0
     0 0 1 0
     0 0 0 1"/>
    </VRUIConnection>
    <VRPNConnection name="spaceNav" address="device0@localhost">
      <Analog id="0" name="channels"/>
    </VRPNConnection>
 </VRConnectionManager>
\end{verbatim}

\begin{itemize}
\item VRConnectionManager can define multiple connection. Each connection can of of type VRUIConnection or VRPNConnection.
\item Each connection has a name associated with it. Also data coming in from these connection can be given specific names.
\item In our example above a \VRUI connection is defined and named \textbf{travel}. We also define a name to the tracking data on this connection calling it \textbf{head}. We can henceforth refer to this data using a dotted notation as follows:
\begin{verbatim}
travel.head
\end{verbatim}
\item Like all data being read into the system can be assigned some virtual names which will be used down the line.
\item Following is a pictorial representation of the process.
\end{itemize}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.9\linewidth]{Images/ParaView_UsersGuide_Connection_Configuration.png}
\label{fig:ConnectionConfiguration1}
\end{center}
\end{figure}

\paragraph{Interaction Styles}
Interactor styles define certain fixed sets of interaction that we usually perform in \VE. There are fixed number of hand-coded interaction styles.

\begin{itemize}
\item \textbf{vtkVRStyleTracking} maps tracking data to a certain \ParaView proxy object.
\item \textbf{vtkVRStyleGrabNUpdateMatrix} takes button press and updates some transformation based on incoming tracking data.
\item \textbf{vtkVRStyleGrabNTranslateSliceOrigin} takes a button press and updates a position based on the position of the tracked input.
\item \textbf{vtkVRStyleGrabNRotateSliceNormal} takes a button press and updates a vector based on the orientation of the tracked input.
\end{itemize}

Interactor styles are specified using the following XML format:
\begin{verbatim}
<VRInteractorStyles>
    <Style class="vtkVRStyleTracking" set\_property="RenderView1.HeadPose">
      <Tracker name="travel.head"/>
    </Style>
    <Style class="vtkVRStyleGrabNUpdateMatrix" set_property="RenderView1.WandPose">
      <Tracker name="travel.wand"/>
      <Button name="travel.1"/>
      <MatrixProperty name="RenderView1.WandPose"/>
    </Style>
    <Style class="vtkVRStyleGrabNTranslateSliceOrigin" origin="CutFunction.Origin">
      <Button name="travel.2"/>
      <Tracker name="travel.wand"/>
    </Style>
    <Style class="vtkVRStyleGrabNRotateSliceNormal" normal="CutFunction.Normal">
      <Button name="travel.3"/>
      <Tracker name="travel.wand"/>
    </Style>
  </VRInteractorStyles>
\end{verbatim}
The following is a pictorial representation of the entire process:

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.9\linewidth]{Images/ParaView_UsersGuide_VR_Plugin_Workflow.png}
\label{fig:VRPluginWorkflow}
\end{center}
\end{figure}

\paragraph{Piggy-Backing the state file}

\begin{itemize}
\item The VRPlugin uses the state loading mechanism to read its configuration. This was just a quick and dirty way and will most probably be replaced with different method (perhaps a GUI even).
\item More about the state file can be found
\todo{FixReferences}
%http://paraview.org/Wiki/Advanced_State_Management
here.
\item A state file is a representation of the visualization pipeline in PV.
\item So the first step before configuring the inputs is to load and create a scene in PV.
\item Export the corresponding state into a state file (config.pvsm). A state file has an extention *.pvsm with content as follows
\begin{verbatim}
<ParaView>
  <ServerManagerState version="3.11.1">
     .....
     .....
  </ServerManagerState>
  <ViewManager>
    .....
  </ViewManager>
</ParaView>
\end{verbatim}
\item We extend the state file to introduce the VRConnectionManager and VRInteractorStyles tags as follows:
\begin{verbatim}
<ParaView>
  <ServerManagerState version="3.11.1">
     .....
     .....
  </ServerManagerState>

  <!-- Start Append state file ----- -->
  <VRConnectionManager>
    <VRUIConnection ...
  </VRConnectionManager>

  <VRInteractorStyles>
    <Style ...
  </VRInteractorStyles>
  <!-- End Append state file -------- -->

  <ViewManager>
    .....
  </ViewManager>
</ParaView>
\end{verbatim}
\item Now our state file not only contains the scene but also the device interaction configuration (\directory{config.pvsm}).
\end{itemize}

\paragraph{Start Server}
On a terminal run the server (For example we want to run 6 processes each driving one display in the cave. Change the number of process according to the number of displays).
\begin{verbatim}
\# PV_ICET_WINDOW_BORDERS=1 mpiexec -np 6 ./pvserver  /path/to/ConfigFile.pvx
\end{verbatim}
Note: \texttt{PV\_ICET\_WINDOW\_BORDERS=1} disables the full-screen mode and instead opens up a 400x400 window. Remove this environment variable to work in full-screen mode.

\paragraph{Start Client}
\begin{itemize}
  \item Open another terminal to run the client. (note: this client connects to the server which open up 6 windows according to the config given in \texttt{cave.pvx}). Change the stereo-type according to preference (see \texttt{\# ./paraview --help}).
\begin{verbatim}
\# ./paraview --stereo --stereo-type=Anaglyph --server=localhost
\end{verbatim}
\item \texttt{--server=localhost} connects the client to the server.
\item Enable the VRPlugin on the client.
\item Load the updated state file (\directory{config.pvsm}) specifying \VRPN or \VRUI client connection and interaction styles.
\end{itemize}
